
// 
const listArray = [];
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
        data.map((elem) => {
            listArray.push(elem.title);
        });
    });
    console.log(listArray);

// 
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json)=> console.log(json))

// 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method  : 'GET',
   headers : {
      'Content-Type' : 'application/json'
   }
})
.then(response => response.json())
.then(json => console.log("The item " + json.title + " on the list has a status of " + json.completed));

// 
fetch('https://jsonplaceholder.typicode.com/todos',{

		method: 'POST',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
			userId: 1,
			completed: false
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// 
fetch('https://jsonplaceholder.typicode.com/todos/1',{

		method: 'PUT',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated To Do List Item',
			userId: 1,
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			status: "Pending"
		})

	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-type':'application/json'
		},
		body: JSON.stringify({
			title: 'delectus aut autem',
			completed: false,
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	})
	.then((response)=> response.json())
	.then((json)=> console.log(json));	


// 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'DELETE'
	})